import { HeroAreaAppend } from "./scripts/HeroArea";
import { NavigationAppend } from "./scripts/Navigation";
import { SliderAppend } from "./scripts/Slider";
import { FeatureSectionAppend } from "./scripts/Feature";
import { ProductsAppend } from "./scripts/Products";
import { BoxingSectionAppend } from "./scripts/Boxing";
import { CtaSectionAppend } from "./scripts/Cta";
import { FooterAppend } from "./scripts/Footer";

function buildPage() {
  NavigationAppend();
  HeroAreaAppend();
  SliderAppend();
  FeatureSectionAppend();
  ProductsAppend();
  BoxingSectionAppend();
  CtaSectionAppend();
  FooterAppend();
}

window.addEventListener("load", () => {
  buildPage();
});
