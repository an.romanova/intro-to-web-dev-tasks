/* eslint-disable import/prefer-default-export */
import { body } from "./const";
import logo from "../assets/icons/logo.svg";
import search from "../assets/icons/search.svg";
import box from "../assets/icons/box.svg";
import user from "../assets/icons/user.svg";
import menu from "../assets/icons/menu.svg";

export const NavigationAppend = () => {
  const Navigation = document.createElement("section");
  Navigation.classList.add("header");
  body.append(Navigation);
  Navigation.innerHTML = `
  <div class="container">
  <div class="header__content">
    <a href="/" class="logo">
      <img src="${logo}" alt="logo" />
    </a>
    <nav class="nav">
      <ul class="nav__list">
        <li class="nav__item">
          <a href="#NEEDS-LINK" class="icon icon--circle">
            <img
              src="${search}"
              aria-label="search item"
              alt=""
            />
          </a>
        </li>
        <li class="nav__item">
          <a href="#NEEDS-LINK" class="icon icon--circle"
            ><img
              src="${box}"
              aria-label="visit your shopping cart"
              alt=""
            />
          </a>
        </li>
        <li class="nav__item">
          <a href="#NEEDS-LINK" class="icon icon--circle">
            <img
              src="${user}"
              aria-label="login"
              alt=""
            />
          </a>
        </li>
      </ul>
    </nav>
    <span class="mobile-menu">
      <button type="button">
        <img src="${menu}" alt="mobile menu" />
      </button>
    </span>
  </div>
</div>
  `;
  return Navigation;
};
