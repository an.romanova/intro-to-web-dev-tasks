/* eslint-disable import/prefer-default-export */
import { body } from "./const";
import logo from "../assets/icons/logo.svg";
import instagram from "../assets/icons/instagram.svg";
import twitter from "../assets/icons/twitter.svg";
import facebook from "../assets/icons/facebook.svg";

export const FooterAppend = () => {
  const Footer = document.createElement("footer");
  Footer.classList.add("footer");
  body.append(Footer);
  Footer.innerHTML = `
  <div class="container container--footer">
  <div class="footer__content">
    <a href="/" class="logo logo--footer">
      <img src="${logo}" alt="logo" />
    </a>
    <nav class="nav nav--footer">
      <ul class="nav__list">
        <li class="nav__item">
          <a href="#NEEDS-LINK" class="nav__link">Home</a>
        </li>
        <li class="nav__item">
          <a href="#NEEDS-LINK" class="nav__link">About</a>
        </li>
        <li class="nav__item">
          <a href="#NEEDS-LINK" class="nav__link">Product</a>
        </li>
      </ul>
    </nav>
    <ul class="social">
      <li class="social__item">
        <a
          href="https://www.instagram.com/"
          class="social__link"
          aria-label="visit our instagram account"
          target="_blank"
          ><img src="${instagram}" alt="instagram logo"
        /></a>
      </li>
      <li class="social__item">
        <a
          href="https://twitter.com/"
          class="social__link"
          aria-label="visit our twitter account"
          target="_blank"
          ><img src="${twitter}" alt="twitter logo"
        /></a>
      </li>
      <li class="social__item">
        <a
          href="https://facebook.com/"
          class="social__link"
          aria-label="visit our facebook account"
          target="_blank"
          ><img src="${facebook}" alt="facebook logo"
        /></a>
      </li>
    </ul>
  </div>
</div>
  `;
  return Footer;
};
