/* eslint-disable import/prefer-default-export */
import { body } from "./const";
import boxing from "../assets/images/boxing.png";

export const BoxingSectionAppend = () => {
  const BoxingSection = document.createElement("section");
  BoxingSection.classList.add("section");
  body.append(BoxingSection);
  BoxingSection.innerHTML = `
  <div class="container">
  <div class="split-content">
    <div class="split-content__column split-content__column--large">
      <img
        class="image"
        src="${boxing}"
        alt="case for headphones image"
      />
    </div>
    <div class="split-content__column split-content__column--small">
      <h2 class="heading heading--2">Whatever you get in the box</h2>
      <ul class="list list--arrow-point">
        <li class="list__el">5A Charger</li>
        <li class="list__el">Extra battery</li>
        <li class="list__el">Sophisticated bag</li>
        <li class="list__el">User manual guide</li>
      </ul>
    </div>
  </div>
</div>
  `;
  return BoxingSection;
};
