/* eslint-disable import/prefer-default-export */
import { body } from "./const";
import model1 from "../assets/images/clour-01.png";
import model2 from "../assets/images/clour-02.png";
import model3 from "../assets/images/clour-03.png";
import arrowRight from "../assets/icons/arrow-right.svg";
import arrowLeft from "../assets/icons/arrow-left.svg";

export const SliderAppend = () => {
  const Slider = document.createElement("section");
  Slider.classList.add("section", "section--slider");
  body.append(Slider);
  Slider.innerHTML = `
  <div class="container">
  <h2 class="heading heading--2 heading--align-center">
    Our Latest colour collection 2021
  </h2>
  <div class="slider">
    <div class="slider__wrapper">
      <button type="button" class="slider__action">
        <img
          src="${arrowLeft}"
          aria-label="show previous"
          alt=""
        />
      </button>
      <div class="slider__items">
        <div class="slider__item">
          <img src="${model2}" alt="model 2" />
        </div>
        <div class="slider__item slider__item--active">
          <img src="${model1}" alt="model 1" />
        </div>
        <div class="slider__item">
          <img src="${model3}" alt="model 3" />
        </div>
      </div>
      <button type="button" class="slider__action">
        <img
          src="${arrowRight}"
          aria-label="show next"
          alt=""
        />
      </button>
    </div>
  </div>
</div>
  `;
  return Slider;
};
