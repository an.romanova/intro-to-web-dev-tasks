/* eslint-disable import/prefer-default-export */
import { body } from "./const";
import product1 from "../assets/images/product-01.png";
import product2 from "../assets/images/product-02.png";
import product3 from "../assets/images/product-03.png";
import cart from "../assets/icons/shopping-cart.svg";
import star from "../assets/icons/star.svg";

export const ProductsAppend = () => {
  const Products = document.createElement("section");
  Products.classList.add("section");
  body.append(Products);
  Products.innerHTML = `
  <div class="container">
  <div class="heading-section">
    <h2 class="heading heading--2">Our Latest Products</h2>
    <p>
      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas
      facilisis nunc ipsum aliquam, ante.
    </p>
  </div>
  <div class="columns columns--3">
    <div class="columns__column">
      <div class="card card--pink">
        <div class="card__top">
          <div class="card__image-container">
            <img
              class="card__image"
              src="${product3}"
              alt="pink headphones"
            />
          </div>
          <div class="card__image-bg"></div>
          <div class="card__cart">
            <img src="${cart}" alt="" />
          </div>
        </div>
        <div class="card__bottom">
          <div class="rating">
            <div class="rating__stars-wrapper">
              <span class="rating__star"
                ><img src="${star}" alt=""
              /></span>
              <span class="rating__star"
                ><img src="${star}" alt=""
              /></span>
              <span class="rating__star"
                ><img src="${star}" alt=""
              /></span>
              <span class="rating__star"
                ><img src="${star}" alt=""
              /></span>
              <span class="rating__star"
                ><img src="${star}" alt=""
              /></span>
            </div>
            <span class="rating__value">4.50</span>
          </div>
          <div class="card__text inline">
            <p class="large">Red Headphone</p>
            <strong class="large">$ 256</strong>
          </div>
        </div>
      </div>
    </div>
    <div class="columns__column">
      <div class="card">
        <div class="card__top">
          <div class="card__image-container">
            <img
              class="card__image"
              src="${product2}"
              alt="pink headphones"
            />
          </div>
          <div class="card__image-bg"></div>
          <div class="card__cart">
            <img src="${cart}" alt="" />
          </div>
        </div>
        <div class="card__bottom">
          <div class="rating">
            <div class="rating__stars-wrapper">
              <span class="rating__star"
                ><img src="${star}" alt=""
              /></span>
              <span class="rating__star"
                ><img src="${star}" alt=""
              /></span>
              <span class="rating__star"
                ><img src="${star}" alt=""
              /></span>
              <span class="rating__star"
                ><img src="${star}" alt=""
              /></span>
              <span class="rating__star"
                ><img src="${star}" alt=""
              /></span>
            </div>
            <span class="rating__value">4.50</span>
          </div>
          <div class="card__text inline">
            <p class="large">Blue Headphones</p>
            <strong class="large">$ 235</strong>
          </div>
        </div>
      </div>
    </div>
    <div class="columns__column">
      <div class="card card--green">
        <div class="card__top">
          <div class="card__image-container">
            <img
              class="card__image"
              src="${product1}"
              alt="pink headphones"
            />
          </div>
          <div class="card__image-bg"></div>
          <div class="card__cart">
            <img src="${cart}" alt="" />
          </div>
        </div>
        <div class="card__bottom">
          <div class="rating">
            <div class="rating__stars-wrapper">
              <span class="rating__star"
                ><img src="${star}" alt=""
              /></span>
              <span class="rating__star"
                ><img src="${star}" alt=""
              /></span>
              <span class="rating__star"
                ><img src="${star}" alt=""
              /></span>
              <span class="rating__star"
                ><img src="${star}" alt=""
              /></span>
              <span class="rating__star"
                ><img src="${star}" alt=""
              /></span>
            </div>
            <span class="rating__value">4.50</span>
          </div>
          <div class="card__text inline">
            <p class="large">Green Headphone</p>
            <strong class="large">$ 245</strong>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
  `;
  return Products;
};
