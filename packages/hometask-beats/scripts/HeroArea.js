/* eslint-disable import/prefer-default-export */
import { body } from "./const";
import hero from "../assets/images/hero.png";

export const HeroAreaAppend = () => {
  const HeroArea = document.createElement("section");
  HeroArea.classList.add("hero");
  body.append(HeroArea);
  HeroArea.innerHTML = `
  <div class="container">
    <div class="hero__container">
      <div class="hero__img">
        <img
          src="${hero}"
          alt="image of red headphones"
        />
      </div>
      <div class="hero__content">
        <p class="subtile">Hear it. Feel it</p>
        <h1 class="heading heading--1">Move with the music</h1>
        <div class="price">
          <span class="price__item">$ 435</span>
          <span class="price__item">$ 465</span>
        </div>
        <a href="#NEEDS-LINK" class="btn btn--white">BUY NOW</a>
      </div>
    </div>
  </div>
  `;
  return HeroArea;
};
