/* eslint-disable import/prefer-default-export */
import { body } from "./const";

export const CtaSectionAppend = () => {
  const CtaSection = document.createElement("section");
  CtaSection.classList.add("section");
  body.append(CtaSection);
  CtaSection.innerHTML = `
  <div class="container">
  <div class="card-subscribe">
    <div class="heading-section">
      <h2 class="heading heading--3">Subscribe</h2>
      <p>Lorem ipsum dolor sit amet, consectetur</p>
    </div>
    <form class="form">
      <div class="form__field">
        <label for="email">email address</label>
        <input
          type="email"
          id="email"
          placeholder="Enter Your email address"
        />
      </div>
      <button type="submit" class="btn btn--accent-color">
        Subscribe
      </button>
    </form>
  </div>
</div>
  `;
  return CtaSection;
};
