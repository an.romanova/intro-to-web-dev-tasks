/* eslint-disable import/prefer-default-export */
import { body } from "./const";
import battery from "../assets/icons/battery.svg";
import bluetooth from "../assets/icons/bluetooth.svg";
import microphone from "../assets/icons/microphone.svg";
import feature from "../assets/images/feature.png";

export const FeatureSectionAppend = () => {
  const FeatureSection = document.createElement("section");
  FeatureSection.classList.add("section");
  body.append(FeatureSection);
  FeatureSection.innerHTML = `
  <div class="container">
  <div class="split-content">
    <div class="split-content__column split-content__column--small">
      <h2 class="heading heading--2">
        Good headphones and loud music is all you need
      </h2>
      <ul class="list list--icon-content">
        <li class="list__el">
          <div class="icon-content">
            <div class="icon-content__icon">
              <img src="${battery}" alt="" />
            </div>
            <div class="icon-content__text">
              <h3 class="heading heading--3">Battery</h3>
              <p>Battery 6.2V-AAC codec</p>
              <a href="#NEEDS-LINK" class="link">Lern More</a>
            </div>
          </div>
        </li>
        <li class="list__el">
          <div class="icon-content">
            <div class="icon-content__icon">
              <img src="${bluetooth}" alt="" />
            </div>
            <div class="icon-content__text">
              <h3 class="heading heading--3">Bluetooth</h3>
              <p>Battery 6.2V-AAC codec</p>
              <a href="#NEEDS-LINK" class="link">Lern More</a>
            </div>
          </div>
        </li>
        <li class="list__el">
          <div class="icon-content">
            <div class="icon-content__icon">
              <img src="${microphone}" alt="" />
            </div>
            <div class="icon-content__text">
              <h3 class="heading heading--3">Microphone</h3>
              <p>Battery 6.2V-AAC codec</p>
              <a href="#NEEDS-LINK" class="link">Lern More</a>
            </div>
          </div>
        </li>
      </ul>
    </div>
    <div class="split-content__column split-content__column--large">
      <img
        class="image"
        src="${feature}"
        alt="model 4"
      />
    </div>
  </div>
</div>
  `;
  return FeatureSection;
};
