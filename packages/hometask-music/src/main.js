import router from "./plugins/router";
import { Footer } from "./components/footer";
import { Header } from "./components/header";

setTimeout(() => {
  document.querySelector("#app").prepend(Header());
  document.querySelector("#app").append(Footer());
});
