/* eslint-disable import/prefer-default-export */
import logo from "../../assets/icons/logo.svg";

export const Header = () => {
  const header = document.createElement("header");
  header.classList.add("header");
  header.innerHTML = `
  <div class="container">
  <div class="header__content">
    <a href="/" class="logo">
      <img src=${logo} alt="Simo logo" />
      <span>Simo</span>
    </a>
    <nav class="nav">
      <ul class="nav-list">
        <li class="nav-list__item"><a href="!#">Discover</a></li>
        <li class="nav-list__item"><a href="!#">Join</a></li>
        <li class="nav-list__item"><a href="!#">Sign In</a></li>
      </ul>
    </nav>
  </div>
</div>
  `;
  return header;
};
