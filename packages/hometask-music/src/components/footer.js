/* eslint-disable import/prefer-default-export */
export const Footer = () => {
  const footer = document.createElement("footer");
  footer.classList.add("footer");
  footer.innerHTML = `
  <div class="container">
  <nav class="nav">
    <ul class="nav-list nav-list--footer">
      <li class="nav-list__item"><a href="!#">About As</a></li>
      <li class="nav-list__item"><a href="!#">Contact</a></li>
      <li class="nav-list__item"><a href="!#">CR Info</a></li>
      <li class="nav-list__item"><a href="!#">Twitter</a></li>
      <li class="nav-list__item"><a href="!#">Facebook</a></li>
    </ul>
  </nav>
</div>
  `;
  return footer;
};
