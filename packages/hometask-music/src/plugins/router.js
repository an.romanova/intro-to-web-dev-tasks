import { Router } from "yourrouter";
import { LandingPage } from "../pages/LandingPage";
import { FeaturePage } from "../pages/FeaturePage";
import { SignUpPage } from "../pages/SignUpPage";

Router.createInstance({
  path404: "/notFound", // name of route with 404 HTTP status code
  renderId: "#router", // Id where the templates will be rendered
});

const router = Router.getInstance();

router.addRoute("/", () => LandingPage);

router.addRoute("/feature", () => FeaturePage);

router.addRoute("/signup", () => SignUpPage);

export default router;
