export const FeaturePage = () =>
  `<main class="main main--feature">
    <div class="container">
      <div class="split-content">
        <div class="split-content__content content">
          <h1 class="heading">Discover new music</h1>
          <div class="btn-group">
            <button class="btn" type="button">Charts</button>
            <button class="btn" type="button">Songs</button>
            <button class="btn" type="button">Artists</button>
          </div>
          <p>
            By joing you can benefit by listening to the latest albums
            released
          </p>
        </div>
        <div class="split-content__content image">
          <img src="./assets/images/music-titles.png" alt="albums covers" />
        </div>
      </div>
    </div>
  </main>`;
