export const LandingPage = () => `
<div class="container">
<div class="content">
  <h1 class="heading">Feel the music</h1>
  <p>Stream over 10 million songs with one click</p>
  <button class="btn" type="button">Join now</button>
</div>
</div>
`;
